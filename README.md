# Chapas Project

## Description

![](https://chapasproject.files.wordpress.com/2014/07/logo_mapa_dos_chapas_200x200.png)

Chapas is the collective term used by the people of Mozambique for privately operated vans and minibuses. These are the primary means of transport in the country.

The project began in August 2013 with the purpose of making a Map of the Chapas of Maputo, the capital of Mozambique, to improve the collective transport.

[Website](https://chapasproject.wordpress.com/)